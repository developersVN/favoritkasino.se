'use strict';

$(document).ready(function () {
    $('.navbar-nav .menu-item-has-children').each(function(index,value){
        $(value).on('click',function(){
            if($(this).hasClass('open')) {
                $(this).removeClass('open');
            } else {
                $('.navbar-nav li').removeClass('open');
                $(this).addClass('open');

            }
        });
    });
    var is_open = false;
    $('.icon-search').click(function(){
        if(is_open) {
            $('.wrap-search').slideUp();
            is_open = false;
        }else {
            $('.wrap-search').slideDown();
            is_open = true;
        }
    })
});