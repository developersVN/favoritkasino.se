<?php
require_once get_template_directory().'/core/ThemeBuilder.php';

function get_theme_builder(){
    return ThemeBuilder::create_builder();
}

function init_theme(){
    $builder = get_theme_builder();
    $builder->init_theme();
    $builder->add_bootstrap_shortcode();
    $builder->create_option_page();
}
add_action('after_setup_theme','init_theme');


function get_theme_logo(){
    $builder = get_theme_builder();
    return $builder->get_theme_logo();
}

function import_theme_styles(){
    $style_list = array(
        'main-style' => array(
            'src' => 'favoritkasino-main.css'
        ),
    );
    $builder = get_theme_builder();
    $builder->import_google_fonts(array('Manuale:400,600,700'));
    $builder->import_assets_style($style_list);
}
add_action("wp_enqueue_scripts","import_theme_styles");

function import_theme_scripts(){
    $script_list = array(
        'vendor-js' => array(
            'src' => 'favoritkasino-vendor.js'
        ),
        'main-js' => array(
            'src' => 'favoritkasino-main.js'
        ),
        
    );
    $builder = get_theme_builder();
    $builder->import_assets_script($script_list);
}
add_action("wp_enqueue_scripts","import_theme_scripts");

function get_page_slider_data(){
   $builder = get_theme_builder();
   return $builder->get_slider_data('website_slider','slider',array('link'));
}

function has_sidebar_widget(){
    $builder = get_theme_builder();
    return $builder->has_sidbar_widget();
}

function body_classes( $classes ) {
    // Adds a class of custom-background-image to sites with a custom background image.
    if ( get_background_image() ) {
        $classes[] = 'custom-background-image';
    }

    // Adds a class of group-blog to sites with more than 1 published author.
    if ( is_multi_author() ) {
        $classes[] = 'group-blog';
    }

    // Adds a class of no-sidebar to sites without active sidebar.
   
    if ( is_active_sidebar( 'sidebar-1' ) ) {
        $classes[] = 'has-sidebar';
    } else {
        $classes[] = 'no-sidebar';
    }

    // Adds a class of hfeed to non-singular pages.
    if ( ! is_singular() ) {
        $classes[] = 'hfeed';
    }

    return $classes;
}
add_filter( 'body_class', 'body_classes' );

function shortcode_casino_boxes(){
    ob_start();
    if(!class_exists('acf')) return ob_get_clean();
    $frontpage_id = get_option('page_on_front' );
    if(have_rows("casino_boxes",$frontpage_id)): 
?>
        <div class="favoritkasino-casino-boxes"> 
            <div class="row">
                <?php 
                    while(have_rows("casino_boxes", $frontpage_id)): the_row(); 
                        $image = get_sub_field("logo");
                        $rate = get_sub_field("rate");
                ?> 
                    <div class="col-xs-12 col-sm-4">
                        <div class="favoritkasino-casino-box">
                            <div class="favoritkasino-casino-logo">
                                 <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
                            </div>
                            <div class="favoritkasino-casino-detail">
                                <ul class="favoritkasino-rate">
                                    <?php for($i=1;$i <= 5; $i++ ):?>
                                        <?php if($i<=$rate):?>
                                            <li class="rated">&#9733;</li>
                                        <?php else:?>
                                            <li></li>
                                        <?php endif;?>
                                    <?php endfor;?>
                                </ul>
                                <div class="description">
                                    <?php echo get_sub_field("description"); ?>
                                </div>
                                <a class="btn btn-primary" href="<?php echo get_sub_field("besok"); ?>"><?php echo get_sub_field_object("besok")['label'] ?> >></a>
                            </div>
                        </div>
                    </div> 
                <?php endwhile; ?>
            </div>
        </div>
<?php
    endif;
    return ob_get_clean();
}
add_shortcode( 'casino-boxes', 'shortcode_casino_boxes' );

function get_casino_list_shortcode( $atts ) {
    ob_start();
    if(!class_exists('acf')) return ob_get_clean();
    $frontpage_id = get_option( 'page_on_front' );
    if(have_rows("casino_list",$frontpage_id)): 
        $spelbolag_lbl = get_sub_field_object('spelbolag')['label'];
        $casino_bonus_lbl = get_sub_field_object('casino_bonus')['label'];
        $betyg_lbl = get_sub_field_object('betyg')['label'];
        $link_label = get_sub_field_object('spela_nu')['label'];
?>      
        <table class="favoritkasino-casino-list ">
            <thead>
                <tr>
                    <th><?php echo $spelbolag_lbl  ?></th>
                    <th><?php echo $casino_bonus_lbl ?></th>
                    <th><?php echo $betyg_lbl  ?></th>
                    <th><?php echo $link_label  ?></th>
                </tr>
            </thead>
            <tbody>
            <?php while(have_rows("casino_list", $frontpage_id)): the_row();
                $images = get_sub_field("spelbolag");
                $rates = get_sub_field("betyg");
            ?>
                <tr>
                    <td data-title="<?php echo $spelbolag_lbl  ?>">
                        <img src="<?php echo $images['url']; ?>" alt="<?php echo $images['alt']; ?>">
                    </td>
                    <td data-title="<?php echo $casino_bonus_lbl ?>">
                        <?php echo get_sub_field("casino_bonus"); ?>
                    </td>
                    <td data-title="<?php echo $betyg_lbl  ?>">
                         <ul class="favoritkasino-rate">
                            <?php for($i=1;$i <= 5; $i++ ):?>
                                <?php if($i<=$rates):?>
                                    <li class="favoritkasino-rated"></li>
                                <?php else:?>
                                    <li></li>
                                <?php endif;?>
                            <?php endfor;?>

                        </ul>
                    </td>
                    <td data-title="<?php echo $link_label  ?>">
                        <a class="btn btn-primary" href="<?php echo get_sub_field("spela_nu"); ?>"><?php echo $link_label ?></a>
                    </td>
                </tr>
            <?php endwhile;?>
            </tbody>
        </table>
    <?php endif;
    return ob_get_clean();
}
add_shortcode( 'casino-list', 'get_casino_list_shortcode' );
function get_contact_link(){
  $pages = get_pages(array(
      'post_type'  => 'page',
      'meta_key'   => '_wp_page_template',
      'meta_value' => 'page-contact.php'
  ));
  if(!empty($pages) && $pages[0]->ID) return get_page_link($pages[0]->ID);
  return '#';
}

