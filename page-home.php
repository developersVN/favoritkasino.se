<?php
/**
 *
 * Template Name: Home page
 * This is the template that displays home page.
 *
 */
get_header(); ?>
    <div id="favoritkasino-primary" class="favoritkasino-content-area">
       	<main id="favoritkasino-main" class="favoritkasino-site-main" >
            <?php
                //Get page content
                // Start the loop.
                while ( have_posts() ) : the_post();
            ?>
                <div class="favoritkasino-the-content">
                    <?php
                        the_content();
                    ?>
                </div>
            <?php
            // End the loop.
            endwhile;
            ?>
        </main><!-- .site-main -->
    </div><!-- .content-area -->
<?php get_footer(); ?>